## [1.1.0](https://gitlab.com/Mintoo200/design-system/compare/v1.0.0...v1.1.0) (2021-12-21)


### Features

* **CSS:** Add Theme ([bb57437](https://gitlab.com/Mintoo200/design-system/commit/bb574370e5f9787e525db593d68e66a6801b2713)), closes [12](https://gitlab.com/Mintoo200/design-system/-/issues/12) [12](https://gitlab.com/Mintoo200/design-system/-/issues/12) [Mintoo200](https://gitlab.com/Mintoo200/design-system/-/issues/Mintoo200) [7](https://gitlab.com/Mintoo200/design-system/-/issues/7)

## 1.0.0 (2021-11-27)


### Features

* **Component:** Infinite scroll component ([a5b3c04](https://gitlab.com/Mintoo200/design-system/commit/a5b3c0475deb9d45d1fc58a291c8f26eee66fa38)), closes [2](https://gitlab.com/Mintoo200/design-system/-/issues/2) [2](https://gitlab.com/Mintoo200/design-system/-/issues/2) [Mintoo200](https://gitlab.com/Mintoo200/design-system/-/issues/Mintoo200) [5](https://gitlab.com/Mintoo200/design-system/-/issues/5)
