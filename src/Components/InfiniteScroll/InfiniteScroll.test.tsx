import React from 'react'
import {render, screen} from '@testing-library/react'
import { InfiniteScroll } from './InfiniteScroll'

describe('<InfiniteScroll />', () => {
  it('should render its children unmodified', () => {
    render(
      <InfiniteScroll getMoreResults={() => null}>
        Test
      </InfiniteScroll>
    )

    const content = screen.getByText('Test')
    expect(content).toBeVisible()
  })
  it('should get more results when reaching the threshold', () => {
    // This test does not test the right thing because JSDom does not
    // actually render anything so getClientBoundingRect() always return {0,0,0,0}.
    // The whole content is therefore always entirely in the viewport.

    const getMoreResults = jest.fn()
    render(
      <InfiniteScroll getMoreResults={getMoreResults}>
        <div />
      </InfiniteScroll>
    )

    expect(getMoreResults).toHaveBeenCalledTimes(1)
  })
  it('should get more results when first render already match threshold', () => {
    // This test works because JSDom does not actually render anything
    // so getClientBoundingRect() always return {0,0,0,0}.
    // The whole content is therefore always entirely in the viewport.

    const getMoreResults = jest.fn()
    render(
      <InfiniteScroll getMoreResults={getMoreResults}>
        <div />
      </InfiniteScroll>
    )

    expect(getMoreResults).toHaveBeenCalledTimes(1)
  })
})